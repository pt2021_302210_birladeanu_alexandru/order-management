package model;
/**
 * @author Alexandru-Gabriel Birladeanu, student at Technical University of Cluj-Napoca, alexbarladeanu@gmail.com
 * @since 19/04/2021
 * @version 1.0.1
 * */

public class Client {
    private int id;
    private String name;
    private String email;
    private String address;

    /**
     * used implicitly in the DAO class
     * @param id the id must be unique
     * @param name
     * @param email
     * @param address
     */
    public Client(int id, String name, String email, String address){
        this.id=id;
        this.name=name;
        this.email=email;
        this.address=address;
    }

    /**
     * the id is auto incremented by the server
     * @param name
     * @param email
     * @param address
     */
    public Client(String name, String email, String address){
        this.name=name;
        this.email=email;
        this.address=address;
    }

    /**
     * constructor with no parameters, necessary for the createObjects method in DAO
     */
    public Client(){}

    /**
     * auto generated method with Alt+Insert
     * @return string containing all attributes
     */
    @Override
    public String toString() {
        return "Client{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", address='" + address + '\'' +
                '}';
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getAddress() {
        return address;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
