package model;

import java.text.SimpleDateFormat;
/**
 * @author Alexandru-Gabriel Birladeanu, student at Technical University of Cluj-Napoca, alexbarladeanu@gmail.com
 * @since 19/04/2021
 * @version 1.0.1
 * */
public class Order {
    private int id;
    private int quantity;
    private SimpleDateFormat date=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private int id_c;
    private int id_p;

    /**
     *
     * @param id primary key
     * @param quantity the number of products removed from the products' stock
     * @param date receives CURRENT_TIMESTAMP on the server
     * @param id_c foreign key in order to enable join with the CLIENTS table
     * @param id_p foreign key in order to enable join with the PRODUCTS table
     */
    public Order(int id, int quantity, SimpleDateFormat date, int id_c, int id_p) {
        this.id = id;
        this.quantity = quantity;
        this.date=date;
        this.id_c = id_c;
        this.id_p = id_p;
    }

    /**
     * id is auto incremented
     * @param quantity number of products ordered
     * @param id_c foreign key
     * @param id_p foreign key
     */
    public Order(int quantity, int id_c, int id_p) {
        this.quantity = quantity;
        this.id_c = id_c;
        this.id_p = id_p;
    }

    /**
     * the constructor with no parameters is called by the method createObjects in DAO
     */
    public Order(){}

    /**
     * generated with Alt+Insert
     * @return a string containing all the attributes
     */
    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", quantity=" + quantity +
                ", date='" + date.toPattern() + '\'' +
                ", id_c=" + id_c +
                ", id_p=" + id_p +
                '}';
    }

    public int getId() {
        return id;
    }

    public int getQuantity() {
        return quantity;
    }

    public SimpleDateFormat getDate() {
        return date;
    }

    public int getId_c() {
        return id_c;
    }

    public int getId_p() {
        return id_p;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public void setDate(SimpleDateFormat date) {
        this.date = date;
    }

    public void setId_c(int id_c) {
        this.id_c = id_c;
    }

    public void setId_p(int id_p) {
        this.id_p = id_p;
    }
}
