package businessLogic;

import businessLogic.validators.EmailValidator;
import businessLogic.validators.Validator;
import dao.ClientDAO;
import model.Client;

import java.util.ArrayList;
import java.util.List;
/**
 * @author Alexandru-Gabriel Birladeanu, student at Technical University of Cluj-Napoca, alexbarladeanu@gmail.com
 * @since 19/04/2021
 * @version 1.0.1
 * */
public class ClientBLL {
    private final List<Validator<Client>> validators;
    private ClientDAO clientDao;

    /**
     * the constructor instantiates the validator list and the ClientDAO object from the DAO package
     */
    public ClientBLL() {
        validators = new ArrayList<Validator<Client>>();
        validators.add(new EmailValidator());
        clientDao = new ClientDAO();
    }

    /**
     *
     * @param id the ID attribute of the client we must find
     * @return the client found or null if no client has the given id
     */
    public Client findClientById(int id) {
        Client client = clientDao.findById(id);
        if (client == null) {
            return null;
            //throw new NoSuchElementException("The client with id =" + id + " was not found!");

        }
        return client;
    }

    /**
     * override method from DAO
     * @param client the newly created client which will be inserted in the database
     * @return the created client if the insertion is successful or null otherwise
     */
    public Client insertClient(Client client) {
        for (Validator<Client> v : validators) {
            v.validate(client);
        }
        return clientDao.insert(client);
    }

    /**
     *  override method from DAO
     * @return a list of all clients in the table
     */
    public List<Client> findAll() {
        return clientDao.findAll();
    }

    /**
     *
     * @param id the id of the client which will be deleted
     * @return 1 if the deletion is successful or -1 otherwise
     */
    public int delete(int id) {
        return clientDao.delete(id);
    }

    /**
     *
     * @param client the newly created client which will be inserted in the table
     * @param id the id of the former client which will be replaced with the newly created one
     * @return the client or null
     */
    public Client update(Client client, int id) {
        for (Validator<Client> v : validators) {
            v.validate(client);
        }
        return clientDao.update(client, id);
    }

    /**
     * override method from DAO
     * @return an array of String objects which represent the header of the table
     */
    public String[] retrieveColumns() {
        return clientDao.retrieveColumns();
    }

    /**
     * override method from DAO
     * @return a matrix of objects which represent the values in all of the fields in the CLIENTS table
     */
    public Object[][] retrieveDataForFindAll() {
        return clientDao.retrieveDataForFindAll();
    }

    /**
     * override method from DAO
     * @param id the id of the client we are searching
     * @return the values of all the fields from the row where the id belongs to the client we are searching
     */
    public Object[][] retrieveDataForFindById(int id) {
        return clientDao.retrieveDataForFindById(id);
    }
}
