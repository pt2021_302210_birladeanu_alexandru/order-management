package businessLogic;

import dao.ProductDAO;
import model.Product;

import java.util.List;
/**
 * @author Alexandru-Gabriel Birladeanu, student at Technical University of Cluj-Napoca, alexbarladeanu@gmail.com
 * @since 19/04/2021
 * @version 1.0.1
 * */
public class ProductBLL {
    private ProductDAO productDAO;

    /**
     * similar to the ClientBLL class, only the methods and instructions are implemented for the PRODUCTS table
     */
    public ProductBLL() {
        productDAO = new ProductDAO();
    }

    public Product findById(int id) {
        Product product = productDAO.findById(id);
        if (product == null) {
            System.out.println("Not found!");
            return null;
            //throw new NoSuchElementException("The product with id =" + id + " was not found!");
        }
        return product;
    }

    public Product insert(Product product) {
        return productDAO.insert(product);
    }

    public List<Product> findAll() {
        return productDAO.findAll();
    }

    public int delete(int id) {
        return productDAO.delete(id);
    }

    public Product update(Product product, int id) {
        return productDAO.update(product, id);
    }

    public String[] retrieveColumns() {
        return productDAO.retrieveColumns();
    }

    public Object[][] retrieveDataForFindAll() {
        return productDAO.retrieveDataForFindAll();
    }

    public Object[][] retrieveDataForFindById(int id) {
        return productDAO.retrieveDataForFindById(id);
    }
}
