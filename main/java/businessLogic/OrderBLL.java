package businessLogic;

import dao.OrderDAO;
import model.Order;

import java.util.List;
/**
 * @author Alexandru-Gabriel Birladeanu, student at Technical University of Cluj-Napoca, alexbarladeanu@gmail.com
 * @since 19/04/2021
 * @version 1.0.1
 * */
public class OrderBLL {
    private OrderDAO orderDAO;

    /**
     * similar to the ClientBLL class, only the methods and instructions are implemented for the ORDERS table
     */
    public OrderBLL(){
        orderDAO=new OrderDAO();
    }

    public Order findById(int id) {
        Order order = orderDAO.findById(id);
        if (order == null) {
            System.out.println("NOT FOUND!");
            return null;
            //throw new NoSuchElementException("The order with id =" + id + " was not found!");
        }
        return order;
    }
    public Order insert(Order order) {
        return orderDAO.insert(order);
    }
    public List<Order> findAll(){
        return orderDAO.findAll();
    }
    public int delete(int id){
        return orderDAO.delete(id);
    }
    public Order update(Order order, int id){
        return orderDAO.update(order, id);
    }
    public String[] retrieveColumns(){
        return orderDAO.retrieveColumns();
    }
    public Object[][] retrieveDataForFindAll(){
        return orderDAO.retrieveDataForFindAll();
    }
    public Object[][] retrieveDataForFindById(int id) {
        return orderDAO.retrieveDataForFindById(id);
    }
}
