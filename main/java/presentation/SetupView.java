package presentation;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionListener;

public class SetupView extends JFrame {
    private final JPanel contentPane;
    private final JComboBox comboBox;
    private final JButton okButton;

    public SetupView(){
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 300, 158);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        JLabel tableSelectionLabel = new JLabel("TABLE SELECTION");
        tableSelectionLabel.setFont(new Font("Constantia", Font.BOLD, 13));
        tableSelectionLabel.setBounds(80, 10, 120, 30);
        contentPane.add(tableSelectionLabel);

        TableChoice[] options = new TableChoice[] {TableChoice.CLIENTS, TableChoice.PRODUCTS, TableChoice.ORDERS};
        comboBox = new JComboBox<>(options);
        comboBox.setBounds(80, 45, 120, 21);
        contentPane.add(comboBox);

        okButton = new JButton("OK");
        okButton.setBounds(110, 85, 60, 21);
        contentPane.add(okButton);
        this.setVisible(true);
    }
    public void okButtonListener(ActionListener actionListener) {
        this.okButton.addActionListener(actionListener);
    }
    public TableChoice getComboInput() {
        return (TableChoice) this.comboBox.getSelectedItem();
    }
}
