package presentation;


import businessLogic.ClientBLL;
import businessLogic.OrderBLL;
import businessLogic.ProductBLL;
import model.Client;
import model.Order;
import model.Product;

import javax.swing.*;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class Controller {
    SetupView setupView;
    QueryView queryView;
    ClientBLL clientBLL;
    OrderBLL orderBLL;
    ProductBLL productBLL;

    public Controller() {
        setupView = new SetupView();
        setupView.okButtonListener(e -> {
            switch (setupView.getComboInput()) {
                case CLIENTS -> {
                    queryView = new QueryView("CLIENTS");
                    clientBLL = new ClientBLL();
                    continueExecution();
                }
                case PRODUCTS -> {
                    queryView = new QueryView("PRODUCTS");
                    productBLL = new ProductBLL();
                    continueExecution();
                }
                default -> {
                    queryView = new QueryView("ORDERS");
                    orderBLL = new OrderBLL();
                    continueExecution();
                }
            }
        });
    }
    private void continueExecution(){
        queryView.executeButtonListener(e -> {
            if(queryView.getFindAllButton().isSelected()){
                findAll();
            }
            if(queryView.getFindByIdButton().isSelected()){
                try {
                    findById(Integer.parseInt(queryView.getIdTextField()));
                } catch(NumberFormatException nfe){
                    JOptionPane.showMessageDialog(queryView, "Invalid ID!");
                }
            }
            if(queryView.getDeleteButton().isSelected()){
                try {
                    delete(Integer.parseInt(queryView.getIdTextField()));
                } catch(NumberFormatException nfe){
                    JOptionPane.showMessageDialog(queryView, "Invalid ID!");
                }
            }
            if(queryView.getInsertButton().isSelected()){
                insert();

            }
            if(queryView.getUpdateButton().isSelected()){
                try{
                    update(Integer.parseInt(queryView.getIdTextField()));
                } catch(NumberFormatException nfe){
                    JOptionPane.showMessageDialog(queryView, "Invalid ID!");
                }
            }
        });
    }
    private void findAll(){
        String[] columns;
        Object[][] data;
        JFrame tableFrame;
        if(clientBLL!=null){
            columns = clientBLL.retrieveColumns();
            data = clientBLL.retrieveDataForFindAll();
            tableFrame=new JFrame("Clients-All rows");
        }
        else {
            if (productBLL != null) {
                columns = productBLL.retrieveColumns();
                data = productBLL.retrieveDataForFindAll();
                tableFrame = new JFrame("Products-All rows");
            }
            else {
                columns = orderBLL.retrieveColumns();
                data = orderBLL.retrieveDataForFindAll();
                tableFrame = new JFrame("Order-All rows");
            }
        }
        JTable jt = new JTable(data, columns);
        JScrollPane jsp = new JScrollPane(jt);
        tableFrame.add(jsp);
        tableFrame.setBounds(500, 400, 300, 200);
        tableFrame.setVisible(true);
    }
    private void findById(int id){
        String[] columns;
        Object[][] data;
        JFrame tableFrame;
        if(clientBLL!=null){
            columns = clientBLL.retrieveColumns();
            data = clientBLL.retrieveDataForFindById(id);
            tableFrame=new JFrame("Client with ID="+id);
        }
        else {
            if (productBLL != null) {
                columns = productBLL.retrieveColumns();
                data = productBLL.retrieveDataForFindById(id);
                tableFrame=new JFrame("Product with ID="+id);
            }
            else {
                columns = orderBLL.retrieveColumns();
                data = orderBLL.retrieveDataForFindById(id);
                tableFrame=new JFrame("Order with ID="+id);
            }
        }
        JTable jt=new JTable(data, columns);
        JScrollPane jsp=new JScrollPane(jt);
        tableFrame.add(jsp);
        tableFrame.setBounds(500, 400, 300, 200);
        tableFrame.setVisible(true);
    }
    private void delete(int id){
        if(clientBLL!=null){
            clientBLL.delete(id);
        }
        else{
            if(productBLL!=null){
                productBLL.delete(id);
            }
            else{
                orderBLL.delete(id);
            }
        }
        findAll();
    }
    private void insert(){
        if(clientBLL!=null){
            clientBLL.insertClient(new Client(queryView.getColumnTextFields()[0].getText(), queryView.getColumnTextFields()[1].getText(), queryView.getColumnTextFields()[2].getText()));
        }
        else {
            if (productBLL != null) {
                try {
                    productBLL.insert(new Product(queryView.getColumnTextFields()[0].getText(), Float.parseFloat(queryView.getColumnTextFields()[1].getText()), Integer.parseInt(queryView.getColumnTextFields()[2].getText())));
                } catch(NumberFormatException nfe){
                    JOptionPane.showMessageDialog(queryView, "Invalid values!");
                }
            }
            else {
                try {
                    int id_p=Integer.parseInt(queryView.getColumnTextFields()[2].getText());
                    int orderQuantity=Integer.parseInt(queryView.getColumnTextFields()[0].getText());
                    ProductBLL productBLLToDecrementStock=new ProductBLL();
                    Product product=productBLLToDecrementStock.findById(id_p);
                    if(product!=null) {
                        int newStock = product.getStock() - orderQuantity;
                        if (newStock >= 0) {
                            product.setStock(newStock);
                            productBLLToDecrementStock.update(product, product.getId());
                            Order order = new Order(orderQuantity, Integer.parseInt(queryView.getColumnTextFields()[1].getText()), id_p);
                            orderBLL.insert(order);
                            printBill(order, new ClientBLL(), new ProductBLL());
                        }
                    }
                    else{
                        JOptionPane.showMessageDialog(queryView, "Not enough products in stock!");
                    }
                } catch(NumberFormatException nfe){
                    JOptionPane.showMessageDialog(queryView, "Invalid values!");
                }
            }
        }
    }
    private void update(int id){
        if(clientBLL!=null){
            clientBLL.update(new Client(queryView.getColumnTextFields()[0].getText(), queryView.getColumnTextFields()[1].getText(), queryView.getColumnTextFields()[2].getText()), id);
        }
        else {
            if (productBLL != null) {
                try {
                    productBLL.update(new Product(queryView.getColumnTextFields()[0].getText(), Float.parseFloat(queryView.getColumnTextFields()[1].getText()), Integer.parseInt(queryView.getColumnTextFields()[2].getText())), id);
                } catch(NumberFormatException nfe){
                    JOptionPane.showMessageDialog(queryView, "Invalid values!");
                }
            }
            else {
                try {
                    int id_p=Integer.parseInt(queryView.getColumnTextFields()[2].getText());
                    int orderQuantity=Integer.parseInt(queryView.getColumnTextFields()[0].getText());
                    ProductBLL productBLLToDecrementStock=new ProductBLL();
                    Product product=productBLLToDecrementStock.findById(id_p);
                    Order oldOrder= orderBLL.findById(id);
                    if(product!=null) {
                        int newStock = product.getStock() - (orderQuantity - oldOrder.getQuantity());
                        if(newStock>=0) {
                            product.setStock(newStock);
                            productBLLToDecrementStock.update(product, product.getId());
                            Order order=new Order(Integer.parseInt(queryView.getColumnTextFields()[0].getText()), Integer.parseInt(queryView.getColumnTextFields()[1].getText()), Integer.parseInt(queryView.getColumnTextFields()[2].getText()));
                            orderBLL.update(order, id);
                            printBill(order, new ClientBLL(), new ProductBLL());
                        }
                    }
                } catch(NumberFormatException nfe){
                    JOptionPane.showMessageDialog(queryView, "Invalid values!");
                }
            }
        }
    }
    private void printBill(Order order, ClientBLL cBLL, ProductBLL pBLL) {
        PrintWriter pw = null;
        int id = order.getId();
        System.out.println(id);
        try {
            pw = new PrintWriter("Last order.txt");
            Client c = cBLL.findClientById(order.getId_c());
            Product p = pBLL.findById(order.getId_p());
            float totalCost=order.getQuantity()*p.getPrice();
            pw.println("LAST ORDER:\nCLIENT: "+ c.toString()+"\nHAS ORDERED "+ order.getQuantity()+"\nPRODUCT(S) OF TYPE: "+p.toString()+"\nTOTAL COST: "+totalCost);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        if (pw != null) {
            pw.close();
        }

    }
}
