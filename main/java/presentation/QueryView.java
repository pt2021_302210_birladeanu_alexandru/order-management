package presentation;

import dao.ClientDAO;
import dao.DAO;
import dao.OrderDAO;
import dao.ProductDAO;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class QueryView extends JFrame implements ActionListener{
    private final JPanel contentPane;
    private final JTextField idTextField;
    private final JRadioButton findAllButton;
    private final JRadioButton findByIdButton;
    private final JRadioButton deleteButton;
    private final JRadioButton updateButton;
    private final JRadioButton insertButton;
    private final JButton executeButton;
    private final JLabel idLabel;
    private final JLabel[] columnLabels;
    private final JTextField[] columnTextFields;
    private final JPanel rightPanel;
    private DAO dao;

    public QueryView(String tableName) {
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setTitle(tableName);
        setBounds(100, 100, 400, 350);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);
        switch (tableName) {
            case "CLIENTS" -> dao = new ClientDAO();
            case "PRODUCTS" -> dao = new ProductDAO();
            case "ORDERS" -> dao = new OrderDAO();
        }

        findAllButton = new JRadioButton("Show all rows");
        findAllButton.setBackground(Color.WHITE);
        findAllButton.setBounds(30, 37, 128, 21);
        contentPane.add(findAllButton);

        findByIdButton = new JRadioButton("Find row by ID");
        findByIdButton.setBackground(Color.WHITE);
        findByIdButton.setBounds(30, 60, 128, 21);
        contentPane.add(findByIdButton);

        deleteButton = new JRadioButton("Delete row by ID");
        deleteButton.setBackground(Color.WHITE);
        deleteButton.setBounds(30, 83, 128, 21);
        contentPane.add(deleteButton);

        updateButton = new JRadioButton("Update row by ID");
        updateButton.setBackground(Color.WHITE);
        updateButton.setBounds(30, 106, 128, 21);
        contentPane.add(updateButton);

        insertButton = new JRadioButton("Insert row");
        insertButton.setBackground(Color.WHITE);
        insertButton.setBounds(30, 129, 103, 21);
        contentPane.add(insertButton);

        ButtonGroup g = new ButtonGroup();
        g.add(findAllButton);
        g.add(findByIdButton);
        g.add(deleteButton);
        g.add(updateButton);
        g.add(insertButton);

        idLabel = new JLabel("ID ");
        idLabel.setBounds(49, 156, 45, 13);
        contentPane.add(idLabel);

        idTextField = new JTextField();
        idTextField.setBounds(70, 153, 45, 19);
        contentPane.add(idTextField);
        idTextField.setColumns(10);

        executeButton = new JButton("EXECUTE");
        executeButton.setBounds(30, 212, 103, 21);
        contentPane.add(executeButton);

        JPanel leftPanel = new JPanel();
        leftPanel.setBounds(14, 27, 150, 221);
        contentPane.add(leftPanel);
        leftPanel.setBackground(Color.WHITE);

        findAllButton.addActionListener(this);
        findByIdButton.addActionListener(this);
        deleteButton.addActionListener(this);
        updateButton.addActionListener(this);
        insertButton.addActionListener(this);

        columnLabels = new JLabel[3];
        for(int i=0; i<3; i++){
            columnLabels[i]=new JLabel("quantity");
            columnLabels[i].setBounds(174, 41+25*i, 60, 13);
            contentPane.add(columnLabels[i]);
        }
        columnTextFields= new JTextField[3];
        for(int i=0; i<3; i++){
            columnTextFields[i]=new JTextField();
            columnTextFields[i].setBounds(234, 38+25*i, 120, 19);
            contentPane.add(columnTextFields[i]);
            columnTextFields[i].setColumns(10);
        }
        rightPanel = new JPanel();
        rightPanel.setBounds(170, 27, 190, 120);
        contentPane.add(rightPanel);
        rightPanel.setBackground(Color.WHITE);

        idLabel.setVisible(false);
        idTextField.setVisible(false);
        for(JLabel l:columnLabels){
            l.setVisible(false);
        }
        for(JTextField t:columnTextFields){
            t.setVisible(false);
        }
        rightPanel.setVisible(false);
        this.setVisible(true);
    }

    public void executeButtonListener(ActionListener actionListener){
        executeButton.addActionListener(actionListener);
    }
    public String getIdTextField() {
        return idTextField.getText();
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        if(findAllButton.isSelected() || insertButton.isSelected()){
            idTextField.setVisible(false);
            idLabel.setVisible(false);
        }
        else{
            idTextField.setVisible(true);
            idLabel.setVisible(true);
        }
        setColumnLabelsAndTextFields();
    }

    public JRadioButton getFindAllButton() {
        return findAllButton;
    }

    public JRadioButton getFindByIdButton() {
        return findByIdButton;
    }

    public JRadioButton getDeleteButton() {
        return deleteButton;
    }

    public JRadioButton getUpdateButton() {
        return updateButton;
    }

    public JRadioButton getInsertButton() {
        return insertButton;
    }
    public void setColumnLabelsAndTextFields(){
        if(insertButton.isSelected() || updateButton.isSelected()){
            String[] columns =dao.retrieveColumns();
            int i=0;
            for (String column : columns) {
                if (!(column.equals("id") || column.equals("date"))) {
                    columnLabels[i].setText(column);
                    columnLabels[i].setVisible(true);
                    columnTextFields[i++].setVisible(true);
                }
            }
            rightPanel.setVisible(true);
        }
        else{
            for(JLabel l:columnLabels){
                l.setVisible(false);
            }
            for(JTextField t:columnTextFields){
                t.setVisible(false);
            }
            rightPanel.setVisible(false);
        }
    }

    public JTextField[] getColumnTextFields() {
        return columnTextFields;
    }
}
