package dao;

import model.Product;
/**
 * @author Alexandru-Gabriel Birladeanu, student at Technical University of Cluj-Napoca, alexbarladeanu@gmail.com
 * @since 19/04/2021
 * @version 1.0.1
 * */
public class ProductDAO extends DAO<Product>{
}
