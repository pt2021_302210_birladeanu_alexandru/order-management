package dao;

import connection.ConnectionFactory;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 * @author Alexandru-Gabriel Birladeanu, student at Technical University of Cluj-Napoca, alexbarladeanu@gmail.com
 * @since 19/04/2021
 * @version 1.0.1
 * */
public abstract class DAO<T> {
    protected static final Logger LOGGER = Logger.getLogger(DAO.class.getName());
    private final Class<T> type;

    @SuppressWarnings("unchecked")
    /**
     * we initialize the type attribute with an object of the class with which DAO is instantiated
     */
    public DAO() {
        this.type = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    /**
     *
     * @param column we created the SQL query where column will be the id
     * @return a string which can be used as an SQL query
     */
    private String getSelectQuery(String column) {
        //System.out.println(new StringBuilder().append("SELECT * FROM ").append(type.getSimpleName().toLowerCase().toString()).append("s WHERE ").append(column).append(" = ").append("?").append(";").toString());
        return "SELECT * FROM " + type.getSimpleName().toLowerCase() + "s WHERE " + column + " = " + "?" + ";";
    }

    /**
     *
     * @return a string which can be used as an SQL query to return all rows from a table
     */
    private String getSelectQuery() {
        System.out.println("SELECT * FROM " + type.getSimpleName().toLowerCase() + "s;");
        return "SELECT * FROM " + type.getSimpleName().toLowerCase() + "s;";
    }

    /**
     *
     * @return a string which can be used as an SQL query to delete a row with a certain id
     */
    private String getDeleteQuery() {
        return "DELETE FROM " + type.getSimpleName().toLowerCase() + "s WHERE id = ?";
    }

    /**
     *
     * @return a string which can be used as an SQL query to insert a new row
     */
    private String getInsertQuery() {
        StringBuilder sb = new StringBuilder();
        sb.append("INSERT INTO ").append(type.getSimpleName()).append("s (");
        for (Field field : type.getDeclaredFields()) {
            if (!field.getName().equals("id") && !field.getName().equals("date")) {
                sb.append(field.getName()).append(",");
            }
        }
        sb.deleteCharAt(sb.length() - 1);
        sb.append(") VALUES(?,?,?)");
        System.out.println(sb.toString());
        return sb.toString();
    }

    /**
     *
     * @param id the id of the row we want to update
     * @return a string which can be used as an SQL query which updates one row
     */
    private String getUpdateQuery(int id) {
        StringBuilder sb = new StringBuilder();
        sb.append("UPDATE ").append(type.getSimpleName()).append("s SET ");
        for (Field field : type.getDeclaredFields()) {
            if (!field.getName().equals("id") && !field.getName().equals("date")) {
                sb.append(field.getName()).append("=?,");
            }
        }
        sb.deleteCharAt(sb.length() - 1);
        sb.append(" WHERE id=").append(id);
        System.out.println(sb.toString());
        return sb.toString();
    }

    /**
     * the SQL query prepared above is ran on the server
     * @param id the id of the element which will be deleted
     * @return 1 if the deletion is successful or -1 otherwise
     */
    public int delete(int id) {
        Connection connection = null;
        PreparedStatement statement = null;
        String query = getDeleteQuery();
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            statement.executeUpdate();
            return 1;
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, type.getName() + "DAO:findById " + e.getMessage());
        } finally {
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        return -1;
    }

    /**
     * the SQL query prepared above is ran on the server
     * @return a list of all the objects in the table
     */
    public List<T> findAll() {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String query = getSelectQuery();
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            resultSet = statement.executeQuery();
            return createObjects(resultSet);
        } catch (SQLException | IndexOutOfBoundsException e) {
            LOGGER.log(Level.WARNING, type.getName() + "DAO:findById " + e.getMessage());
        } finally {
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        return null;
    }

    /**
     *
     * @param id the id of the object being searched
     * @return the found object
     */
    public T findById(int id) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String query = getSelectQuery("id");
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            resultSet = statement.executeQuery();
            //resultSet.next();
            return createObjects(resultSet).get(0);
        } catch (SQLException | IndexOutOfBoundsException e) {
            return null;
            //LOGGER.log(Level.WARNING, type.getName() + "DAO:findById " + e.getMessage());

        } finally {
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        //return null;
    }

    /**
     *
     * @param t the object which will be inserted in the table
     * @return the newly created object
     */
    public T insert(T t) {
        Connection connection = null;
        PreparedStatement statement = null;

        String query = getInsertQuery();
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            int i = 1;
            for (Field field : type.getDeclaredFields()) {
                if (!field.getName().equals("id") && !field.getName().equals("date")) {
                    PropertyDescriptor propertyDescriptor = new PropertyDescriptor(field.getName(), type);
                    Method method = propertyDescriptor.getReadMethod();
                    statement.setObject(i++, method.invoke(t));
                }
            }
            statement.executeUpdate();
        } catch (SQLException | IntrospectionException e) {
            LOGGER.log(Level.WARNING, type.getName() + "DAO:insert. " + e.getMessage());
        } catch (InvocationTargetException | IllegalAccessException e) {
            e.printStackTrace();
        } finally {
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        return t;
    }

    /**
     *
     * @param t the object which will be added to the database
     * @param id the id of the row on which the object will be added, to replace the former one which had this ID
     * @return the newly added object
     */
    public T update(T t, int id) {
        Connection connection = null;
        PreparedStatement statement = null;

        String query = getUpdateQuery(id);
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            int i = 1;
            for (Field field : type.getDeclaredFields()) {
                if (!field.getName().equals("id") && !field.getName().equals("date")) {
                    PropertyDescriptor propertyDescriptor = new PropertyDescriptor(field.getName(), type);
                    Method method = propertyDescriptor.getReadMethod();
                    statement.setObject(i++, method.invoke(t));
                }
            }
            statement.executeUpdate();
        } catch (SQLException | IntrospectionException e) {
            LOGGER.log(Level.WARNING, type.getName() + "DAO:update. " + e.getMessage());
        } catch (InvocationTargetException | IllegalAccessException e) {
            e.printStackTrace();
        } finally {
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        return t;
    }

    /**
     * @param resultSet generated after the execution of the SQL query
     * @return he list of model objects of type T
     */
    private List<T> createObjects(ResultSet resultSet) {
        List<T> list = new ArrayList<>();
        Constructor[] ctors = type.getDeclaredConstructors();
        Constructor ctor = null;
        for (Constructor constructor : ctors) {
            ctor = constructor;
            if (ctor.getGenericParameterTypes().length == 0)
                break;
        }
        try {
            while (resultSet.next()) {
                ctor.setAccessible(true);
                T instance = (T) ctor.newInstance();
                for (Field field : type.getDeclaredFields()) {
                    String fieldName = "";
                    if (!field.getName().equals("date")) {
                        fieldName = field.getName();
                        Object value = resultSet.getObject(fieldName);
                        PropertyDescriptor propertyDescriptor = new PropertyDescriptor(fieldName, type);
                        Method method = propertyDescriptor.getWriteMethod();
                        method.invoke(instance, value);
                    }
                }
                list.add(instance);
            }
        } catch (InstantiationException | IllegalAccessException | SecurityException | IllegalArgumentException | InvocationTargetException | SQLException | IntrospectionException e) {
            e.printStackTrace();
        }
        return list;
    }

    /**
     *
     * @return an array of String objects which represent the header of the table
     */
    public String[] retrieveColumns() {
        String[] fields = new String[type.getDeclaredFields().length];
        int i = 0;
        for (Field field : type.getDeclaredFields()) {
            fields[i++] = field.getName();
        }
        return fields;
    }

    /**
     *
     * @return all the data in the table
     */
    public Object[][] retrieveDataForFindAll() {
        Object[][] fields = new Object[findAll().size()][];
        int i = 0;
        for (T obj : findAll()) {
            Object[] objects = new Object[type.getDeclaredFields().length];
            int j = 0;
            for (Field field : type.getDeclaredFields()) {
                try {
                    PropertyDescriptor propertyDescriptor = new PropertyDescriptor(field.getName(), type);
                    Method method = propertyDescriptor.getReadMethod();
                    objects[j++] = method.invoke(obj);
                } catch (IntrospectionException | IllegalAccessException | InvocationTargetException e) {
                    e.printStackTrace();
                }
            }
            fields[i++] = objects;
        }
        return fields;
    }

    /**
     *
     * @param id necessary to find the object
     * @return the fields' values corresponding to the object with the id equal to the parameter
     */
    public Object[][] retrieveDataForFindById(int id) {
        Object[][] fields = new Object[1][];
        int i = 0;
        T obj = findById(id);
        Object[] objects = new Object[type.getDeclaredFields().length];
        int j = 0;
        for (Field field : type.getDeclaredFields()) {
            try {
                PropertyDescriptor propertyDescriptor = new PropertyDescriptor(field.getName(), type);
                Method method = propertyDescriptor.getReadMethod();
                objects[j++] = method.invoke(obj);
            } catch (IntrospectionException | IllegalAccessException | InvocationTargetException e) {
                e.printStackTrace();
            }
        }
        fields[i++] = objects;
        return fields;
    }
}